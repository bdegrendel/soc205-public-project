var map = null;
var censusData = null;
var censusGeometry = null;
var propertyData = null;

var walkDistance = null;

var propertyPolygons = [];

function initMap()
{
  map = new google.maps.Map($('#map')[0], {
    center: { lat: 42.227, lng: -83.590 },
    zoom: 15,
    mapTypeId: 'roadmap'
  });

  addMapOverlays();
}

function addMapOverlays()
{
  if (map != null && censusGeometry != null && propertyData != null)
  {
    var coordinates = [ ];

    // TODO: This assumes only a single object. Also this structure is... odd.
    for (var i = 0; i < censusGeometry.geometry.coordinates[0].length; i++)
    {
      var coordinate = censusGeometry.geometry.coordinates[0][i];
      coordinates.push({ lat : coordinate[1], lng: coordinate[0] });
    }

    var boundry = new google.maps.Polygon({
      paths: coordinates,
      clickable: false,
      strokeColor: '#0000FF',
      strokeOpacity: 0.5,
      strokeWeight: 2,
      fillColor: '#FFFFFF',
      fillOpacity: 0.35
    });
    boundry.setMap(map);


    walkDistance = new google.maps.Circle({
      clickable: false,
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      map: map,
      center: { lat : 42.230233, lng: -83.594777 },
      radius: 1609.34 * .4});

    walkDistance.setMap(map);

    for (var name in propertyData)
    {
      var property = propertyData[name];
      if (property.polygon.length != 0)
      {
        coordinates = [ ];
        for (var i = 0; i < property.polygon.length; i++)
        {
          coordinates.push(property.polygon[i]);
        }
        var polygon = new google.maps.Polygon({
          infoWindow: new google.maps.InfoWindow,
          propertyData: property,
          paths: coordinates,
          strokeColor: '#00FF00',
          strokeOpacity: 0.75,
          strokeWeight: 1,
          fillColor: '#00FF00',
          fillOpacity: 0.5
        });
        polygon.setMap(map);
        polygon.addListener('click', displayPropertyData);
        propertyPolygons.push(polygon);
      }
    }
  }
}

function displayPropertyData(event)
{
  for (var i = 0; i < propertyPolygons.length; i++)
  {
    propertyPolygons[i].infoWindow.close();
  }

  var valuePerAcre = (this.propertyData['land-value'] / this.propertyData['land-acres']).toFixed(2);
  var taxesPerAcre = (this.propertyData['taxes-2015'] / this.propertyData['land-acres']).toFixed(2);

  var contentString = "<h4>" + this.propertyData.address + "</h4><p>";

  contentString += this.propertyData.description + "<br />";
  contentString += "<a href='" + this.propertyData['online-url'] + "'>Online Property Record</a><br />";
  contentString += "</p><p>";

  contentString += "Owner: " + this.propertyData.owner + "<br />";
  contentString += "Lot zone: ";
  if (this.propertyData.zone == "B1")
    contentString += "<a href='https://www.municode.com/library/mi/ypsilanti_charter_township,_(washtenaw_co.)/codes/code_of_ordinances?nodeId=COOR_APXAZO_ARTIXLOBUDI'>B1 - LOCAL BUSINESS DISTRICTS</a>";
  else
    contentString += this.propertyData.zone;
  contentString += "<br />";
  contentString += "</p><p>";

  contentString += "Building size: " + this.propertyData['building-square-feet'] + " sqft<br />";
  contentString += "Lot size: " + this.propertyData['land-acres'] + " acres<br />";
  contentString += "Lot value: $" + this.propertyData['land-value'] + "<br />";
  contentString += "Property taxes (2015): $" + this.propertyData['taxes-2015'] + "<br />";
  contentString += "</p><p>";

  contentString += "Land value/acre: $" + valuePerAcre + "<br />";
  contentString += "Property taxes/acre (2015): $" + taxesPerAcre + "<br />";
  contentString += "</p>";

  this.infoWindow.setContent(contentString);
  this.infoWindow.setPosition(event.latLng);
  this.infoWindow.open(map);
}

function updateGeometry(data)
{
  censusGeometry = data;
  addMapOverlays();
}

function updateCensusData(data)
{
  censusData = data;

  var gidtrs = '';
  var totalPopulationACS = 0;
  var averageIncomeACS = '';
  var unemploymentPercentACS = '';
  var maleToFemaleRatioACS = '';
  var percentVacantACS = '';

  for (var gidtr in censusData)
  {
    var entry = censusData[gidtr];
    if (gidtrs != '') gidtrs += ',';
    gidtrs += gidtr;

    totalPopulationACS += entry['Tot_Population_ACS_09_13'];

    if (averageIncomeACS != '') averageIncomeACS += ', ';
    averageIncomeACS += entry['Aggregate_HH_INC_ACS_09_13'];

    if (unemploymentPercentACS != '') unemploymentPercentACS += ', ';
    unemploymentPercentACS += (entry['pct_Civ_unemp_16p_ACS_09_13']).toFixed(2) + "%";

    if (maleToFemaleRatioACS != '') maleToFemaleRatio += ', ';
    var i = entry['pct_Females_ACS_09_13'];
    i = (100 - i) / i;
    maleToFemaleRatioACS += i.toFixed(2);

    if (percentVacantACS != '') percentVacantACS += ', ';
    percentVacantACS += (entry['pct_Vacant_Units_ACS_09_13']).toFixed(2) + "%";
  }

  $('#gidtrs').html(gidtrs);
  $('#totalPopulationACS').html(totalPopulationACS);
  $('#averageIncomeACS').html(averageIncomeACS);
  $('#unemploymentPercentACS').html(unemploymentPercentACS);
  $('#maleToFemaleRatioACS').html(maleToFemaleRatioACS);
  $('#percentVacantACS').html(percentVacantACS);
}

function updatePropertyData(data)
{
  propertyData = data;
  addMapOverlays();
}

$.getJSON('json/26161411700-PDB-2015.json', updateCensusData);
$.getJSON('json/26161411700-geometry.json', updateGeometry);
$.getJSON('json/property-data.json', updatePropertyData);

$("#census-data-button").click(function() { $("#census-data").toggle(); });
$("#faq-button").click(function() { $("#faq").toggle(); });
$("#poll-data-button").click(function() { $("#poll-data").toggle(); });
$("#proposal-button").click(function() { $("#proposal").toggle(); });
