# SOC 205 Public Project

* Public git repository is available [on bitbucket][bitbucket].
* Website [hosted online][website].

## Purpose

(to be filled in)

## License

See LICENSE.md for licensing information.  tl;dr: data is from online
government records, remaining source code is public domain.

[bitbucket]: https://bitbucket.org/bdegrendel/soc205-public-project
[website]: http://soc205.sirnuke.com/
