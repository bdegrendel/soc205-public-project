#!/usr/bin/env ruby

require 'csv'

if ARGV.count != 2
  puts "Usage: #{$0} input-csv-filename output-json-filename"
  exit 1
end

input_filename = ARGV[0]
output_filename = ARGV[1]

header = nil
line_number = 0
first = true

def numeric?(value)
  true if Float(value) rescue false
end

File.open(output_filename, "w") do |file|
  file.puts "{"
  CSV.foreach(input_filename) do |row|
    line_number += 1

    if line_number == 1
      if row.empty?
        puts "Header has zero entries, is this a proper CSV?"
        exit 1
      end
      header = row
      next
    end

    if row.count != header.count
      puts "Line ##{line_number} has wrong number of fields: got #{row.count} expected #{header.count}; skipping"
      next
    end

    if first
      first = false
    else
      file.puts ","
    end

    file.puts "  \"#{row[0]}\" :\n  {"
    for i in (0..header.count - 1)
      file.print "    \"#{header[i]}\" : "

      if numeric?(row[i])
        file.print row[i]
      else
        file.print "\"#{row[i]}\""
      end

      file.print "," if i != header.count - 1
      file.puts
    end
    file.print "  }"

  end
  file.puts
  file.puts "}"
end
