# Ypsilanti Tract

## Tract GeoID

Full tract IDs are 26161411700 and part of 26161412600.

* 26 -> State (Michigan)
* 161 -> County (Washtenaw)
* 4117/4126 - Tract number
* 00 -> Who knows?  The [formal GEOID specification][geoid-reference] says tracts are six
  digits, but everywhere (including the GEOID examples!) displays them as four.

## 2015 PDB Data

[Available online][pdb-2015-data], contains tract or block level granularity.  Tract data has
a [variety of data][pdb-2015-tract-fields], all of extrapolated from 2010 census (and
presumably other gathered trend data) to fit 2015.

A command along the lines of `grep "GIDTR\|26161411700\|26161412600" PDB_2015_Tract.csv` should be
sufficent to correctly filter whatever tracts are wanted.  Filtering block data should be near
identical.

For the ease of calculations, just going to use 4117.  Our area uses blocks 2006-2011 and
2018-2019, though that shouldn't be a ton of missed people.

## Shape Data

Boundary data can [be found on the Census website][boundary-data] in Shapefile format.  This in
turn can be easily converted to a more usable JSON format using a GDAL, or gdal-bin package in
Ubuntu.  A command like `ogr2ogr -F "GeoJSON" output.json filename.shp` will produce JSON, and can
be easily grep'd by GEOID.

[geoid-reference]: https://www.census.gov/geo/reference/geoidentifiers.html
[pdb-2015-data]: https://www.census.gov/data/developers/data-sets/planning-database.2015.html
[pdb-2015-tract-fields]: http://api.census.gov/data/2015/pdb/tract/variables.html
[boundary-data]: https://www.census.gov/geo/maps-data/data/cbf/cbf_tracts.html
